# Concentration

An iOS game written in Swift and UIKit during my [Stanford's CS193p online course](https://cs193p.sites.stanford.edu/) self-study for "[Mazowsze - stypendia dla uczniów szkół zawodowych](https://stypendiazawodowe.oeiizk.waw.pl/)" project.

![game preview](preview.png)

## Mechanics

Player takes turns, flipping two cards over at a time. If the cards match, then they have a matched pair and the cards are removed from play. If the cards don’t match, both cards are returned to a facedown position. The game is over when all of the cards have been matched.
