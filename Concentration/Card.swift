//
//  Card.swift
//  Concentration
//
//  Created by Marcin Sadowski on 06/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

class Card: Hashable {
    var isFaceUp = false
    var isMatched = false
    var wasSeen = false
    private let identifier: Int
    
    private static var identifierFactory = 0
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    private init(identifier: Int) {
        self.identifier = identifier
    }
    
    init() {
        identifier = Card.getUniqueIdentifier()
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    func getPair() -> Card {
        return Card(identifier: self.identifier)
    }
}
