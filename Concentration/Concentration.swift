//
//  Concentration.swift
//  Concentration
//
//  Created by Marcin Sadowski on 06/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import Foundation

extension Collection {
    var oneAndOnly: Element? { count == 1 ? first : nil }
}

class Concentration {
    private(set) var score = 0
    private(set) var flipCount: Int?
    private(set) var matchedPairs: Int?
    private var lastMatch: Date?
    private var lastMismatch: Date?
    var cards = [Card]()
    
    private var oneAndOnlyFlippedCardIndex: Int? {
        get { cards.indices.filter { cards[$0].isFaceUp }.oneAndOnly }
        set {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    init(numberOfCardPairs: Int) {
        assert(numberOfCardPairs > 0,
               "Concentration.init(numberOfCardPairs: \(numberOfCardPairs)): you have to have at least one pair of cards."
        )
        
        reinitialize(numberOfCardPairs: numberOfCardPairs)
    }
    
    func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "Concentration.chooseCard(at: \(index)): cards array out of bounds.")
        let currentCard = cards[index]
        
        if !(currentCard.isMatched && currentCard.isFaceUp) {
            flipCount! += 1
            
            if let previousMatchedCardIndex = oneAndOnlyFlippedCardIndex, previousMatchedCardIndex != index {
                let previousCard = cards[previousMatchedCardIndex]
                currentCard.isFaceUp = true
                
                if previousCard == currentCard {
                    // Scoring based on time since last match.
                    if lastMatch == nil {
                        lastMatch = Date()
                        score += 10
                    } else {
                        let deltaTime = Date().timeIntervalSince(lastMatch!)
                        lastMatch = Date()
                        let matchScore = Int(15 - deltaTime)
                        score += matchScore > 0 ? matchScore : 1
                    }
                    
                    previousCard.isMatched = true
                    currentCard.isMatched = true
                    matchedPairs! += 1
                } else {
                    if lastMismatch == nil {
                        lastMismatch = Date()
                        score -= 2
                    } else {
                        let deltaTime = Date().timeIntervalSince(lastMismatch!)
                        lastMismatch = Date()
                        if deltaTime < 1.5 {
                            score -= 10
                        } else {
                            score -= 2
                        }
                    }
                    
                    if (previousCard.wasSeen || currentCard.wasSeen) {
                        score -= 3
                    } else {
                        previousCard.wasSeen = true
                        currentCard.wasSeen = true
                    }
                }
            } else {
                oneAndOnlyFlippedCardIndex = index
            }
        }
    }
    
    func reinitialize(numberOfCardPairs: Int) {
        lastMatch = nil
        lastMismatch = nil
        
        flipCount = 0
        score = 0
        cards.removeAll()
        matchedPairs = 0
        
        for _ in 0..<numberOfCardPairs {
            let card = Card()
            cards += [card, card.getPair()]
        }
        
        cards.shuffle()
    }
}
