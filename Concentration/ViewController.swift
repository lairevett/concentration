//
//  ViewController.swift
//  Concentration
//
//  Created by Marcin Sadowski on 06/04/2020.
//  Copyright © 2020 Marcin Sadowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet private weak var flipsLabel: UILabel!
    @IBOutlet private weak var scoreLabel: UILabel!
    @IBOutlet private weak var newGameBtn: UIButton!

    @IBOutlet private var cardButtons: [UIButton]!
    
    private var cardEmojis = [Card:Character]()
    private var currentEmojiTheme: (emojis: String, backgroundColor: UIColor, uiColor: UIColor)?
    private var emojiThemes: [(String, UIColor, UIColor)] = [
        ("😀😁😂🤣😃😄😅😆😉😊", .systemYellow, .yellow),
        ("🌚🌕🌖🌗🌘🌑🌒🌓🌔🌙", .black, .darkGray),
        ("🍏🍎🍐🍊🍋🍌🍉🍇🍓🍈", .yellow, .green),
        ("👅👂👃👣👁👀🧠🦵🦶🦷", .yellow, .systemPink),
        ("🧥👚👕👖👔👗🎩🧢👒🎓", .blue, .brown),
        ("🐶🐱🐭🐹🐰🦊🦝🐻🐼🦘", .green, .yellow)
    ]
    private var numberOfCardPairs: Int { (cardButtons.count + 1) / 2 }

    private lazy var game = Concentration(numberOfCardPairs: numberOfCardPairs)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseNewEmojiTheme()
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        if let cardIndex = cardButtons.firstIndex(of: sender) {
            let card = game.cards[cardIndex]
            
            if cardEmojis[card] == nil {
                let start = currentEmojiTheme!.emojis.startIndex
                let randomIndex = currentEmojiTheme!.emojis.index(start, offsetBy: currentEmojiTheme!.emojis.count.arc4random)
                cardEmojis[card] = currentEmojiTheme!.emojis.remove(at: randomIndex)
            }
            
            game.chooseCard(at: cardIndex)
            updateViewFromModel()
        } else {
            print("Chosen card doesn't exist.")
        }
    }
    
    @IBAction private func touchNewGameBtn() {
        game.reinitialize(numberOfCardPairs: numberOfCardPairs)
        chooseNewEmojiTheme()
        newGameBtn.isHidden = true
        updateViewFromModel()
    }
    
    private func chooseNewEmojiTheme() {
        currentEmojiTheme = emojiThemes[emojiThemes.count.arc4random]
        view.backgroundColor = currentEmojiTheme!.backgroundColor
        
        newGameBtn.setTitleColor(currentEmojiTheme!.uiColor, for: .normal)
        flipsLabel.textColor = currentEmojiTheme!.uiColor
        scoreLabel.textColor = currentEmojiTheme!.uiColor
        
        for cardBtn in cardButtons {
            cardBtn.backgroundColor = currentEmojiTheme!.uiColor
        }
    }
    
    private func updateViewFromModel() {
        flipsLabel.text = "Flips: \(game.flipCount!)"
        scoreLabel.text = "Score: \(game.score)"
        
        for cardIndex in cardButtons.indices {
            let card = game.cards[cardIndex]
            let cardBtn = cardButtons[cardIndex]
            
            if card.isMatched {
                cardBtn.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
                cardBtn.setTitle(nil, for: .normal)
            } else if card.isFaceUp {
                cardBtn.backgroundColor = UIColor.white
                cardBtn.setTitle(String(cardEmojis[card]!), for: .normal)
            } else {
                cardBtn.backgroundColor = currentEmojiTheme!.uiColor
                cardBtn.setTitle(nil, for: .normal)
            }
        }
        
        if game.matchedPairs == numberOfCardPairs {
            newGameBtn.isHidden = false
        }
    }
}

extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return -Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
